# LaTeX project template

Run `make -C tex all` to generate doc in tex/out folder

## Tools
 - Java
 - PlantUML: https://github.com/plantuml/plantuml/releases/latest
 - Graphviz
 - LaTeX:
    - texlive-latex-base 
    - texlive-lang-cyrillic
    - texlive-latex-extra
    - texlive-luatex
    - texlive-science
    - texlive-xetex
    - texlive-pictures
    - biber
 - TikZiT (TikZ diagrams GUI editor ): https://tikzit.github.io/index.html
